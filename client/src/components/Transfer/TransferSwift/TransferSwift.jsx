import React from "react";
import "./transferSwift.css";

const TransferSwift = ({ setTransferType }) => {
  return (
    <div className="transferSwift">
      <div className="transferSwiftWrapper">
        <div className="transferSwiftLeft">
          <div className="transferSwiftLeftDropdownWrapper">
            <select name="" id="" className="transferSwiftLeftDropdownSelect">
              <option value="ermira" defaultChecked>
                Ermira Mehmeti
              </option>
              <option value="ozkan">Ozkan Selcuk</option>
            </select>
            <img
              src={require(`../../../assets/addpage.png`)}
              alt=""
              className="transferSwiftLeftDropdownIcon"
            />
          </div>
          <div className="transferSwiftLeftScanWrapper">
            <select name="" id="" className="transferSwiftLeftScanIDDropdown">
              <option value="id" defaultChecked>
                Identity Card
              </option>
              <option value="ozkan">Ozkan Selcuk</option>
            </select>
            <div className="transferSwiftScanDocumentWrapper">
              Scan Document
            </div>
            <img
              src={require(`../../../assets/qr.png`)}
              alt=""
              className="transferSwiftLeftScanIDDropdownIcon"
            />
          </div>
          <img
            src="https://images.unsplash.com/photo-1506863530036-1efeddceb993?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1644&q=80"
            alt=""
            className="transferSwiftIDCardImg"
          />
          <div className="transferSwiftLeftInputContainer">
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">ID Number</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Expiry Date</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Address</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">City</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">ZIP Code</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Country</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
          </div>
          <div className="transferSwiftCommentContainer">
            <div className="transferSwiftCommentWrapper">
              <label className="transferSwiftLeftLabel">Comment</label>
              <input className="transferSwiftLeftText" />
            </div>
            <img
              src={require(`../../../assets/printer.png`)}
              alt=""
              className="transferSwiftLeftPrintIcon"
            />
          </div>
        </div>
        <div className="transferSwiftLine"></div>
        <div className="transferSwiftRight">
          <div className="transferSwiftRightWrapper">
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">
                Full Name (Receiver)
              </label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <select
                onChange={(event) => setTransferType(event.target.value)}
                className="transferSwiftRightDropdown"
              >
                <option value="swift">Swift</option>
                <option value="cash">Cash</option>
              </select>
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Beneficiary Name</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">
                Beneficiary Address
              </label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Bank Name</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Bank Address</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Country</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">City</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">IBAN</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">SWIFT</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
          </div>
          <div className="transferSwiftRightAmountContainer">
            <div className="transferSwiftRightAmountSendWrapper">
              <img
                src={require(`../../../assets/euro.png`)}
                alt=""
                className="transferSwiftRightAmountSendIcon"
              />
              <div className="transferSwiftRightAmountSendCurrency">
                <p className="transferSwiftRightAmountSendCurrencyP">
                  You Send
                </p>
                <span className="transferSwiftRightAmountSendCurrencySpan">
                  Euro
                </span>
              </div>
              <div className="transferSwiftRightAmountSendCurrencyAmountWrapper">
                <span className="transferSwiftRightAmountSendCurrencyAmountSpan">
                  Amount:
                </span>
                <div className="transferSwiftRightAmountSendCurrencyAmountValue">
                  <span className="transferSwiftRightAmountSendCurrencyAmountValueSpan">
                    2,400.
                  </span>
                  <p className="transferSwiftRightAmountSendCurrencyAmountValueP">
                    00
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="transferSwiftRightFeeContainer">
            <span className="transferSwiftRightFeeSpan">Inner Fee:</span>
            <div className="transferSwiftRightFeeWrapper">0.4%</div>
            <span className="transferSwiftRightFeeSpan">Fee:</span>
            <div className="transferSwiftRightFeeWrapper">0.9%</div>
          </div>
          <div className="transferSwiftRightAmountContainer">
            <div className="transferSwiftRightAmountSendWrapper">
              <img
                src={require(`../../../assets/euro.png`)}
                alt=""
                className="transferSwiftRightAmountSendIcon"
              />
              <div className="transferSwiftRightAmountSendCurrency">
                <p className="transferSwiftRightAmountSendCurrencyP">You Pay</p>
                <span className="transferSwiftRightAmountSendCurrencySpan">
                  Euro
                </span>
              </div>
              <div className="transferSwiftRightAmountSendCurrencyAmountWrapper">
                <span className="transferSwiftRightAmountSendCurrencyAmountSpan">
                  Amount:
                </span>
                <div className="transferSwiftRightAmountSendCurrencyAmountValue">
                  <span className="transferSwiftRightAmountSendCurrencyAmountValueSpan">
                    2,400.
                  </span>
                  <p className="transferSwiftRightAmountSendCurrencyAmountValueP">
                    00
                  </p>
                </div>
              </div>
            </div>
          </div>
          <button className="transferSwiftRightBottomButton">Confirm</button>
        </div>
      </div>
    </div>
  );
};

export default TransferSwift;
