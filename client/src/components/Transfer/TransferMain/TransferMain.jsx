import React from "react";
import "./transferMain.css";

const TransferMain = () => {
  return (
    <div className="transferMain">
      <div className="transferMainWrapper">
        <div className="transferMainLeft">
          <div className="transferSwiftRightWrapper">
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">
                Full Name (Sender)
              </label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">
                Full Name (Receiver)
              </label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <select className="transferSwiftRightDropdown">
                <option value="swift">Identity Card</option>
                <option value="cash">Cash</option>
              </select>
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <select className="transferSwiftRightDropdown">
                <option value="swift">Cash</option>
                <option value="cash">Swift</option>
              </select>
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">ID Number</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">City</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Expiry Date</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Country</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Address</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">City</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Country</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferMainCommentWrapper">
              <label className="transferSwiftLeftLabel">Comment</label>
              <input className="transferSwiftLeftText" />
            </div>
          </div>
        </div>
        <div className="transferMainLine"></div>
        <div className="transferMainRight">
          <div className="transferMainRightTopContainer">
            <div className="transferMainRightTopTransferWrapper">
              <span className="transferMainRightTopTransferSpan">
                Transfer Code
              </span>
              <div className="transferMainRightTopTransferCheckboxWrapper">
                <span className="transferMainRightTopTransferCheckboxSpan">
                  Manual
                </span>
                <input
                  type="checkbox"
                  className="transferMainRightTopTransferCheckboxInput"
                />
              </div>
            </div>
            <div className="transferMainRightTopTransferNumberWrapper">
              <span className="transferMainRightTopTransferNumberSpan">
                ds-dasdag-23ewrdgf-esfdg-21
              </span>
              <img
                src={require(`../../../assets/qr.png`)}
                alt=""
                className="transferMainRightTopTransferNumberIcon"
              />
            </div>
          </div>
          <div className="transferMainRightWrapper">
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">ID Number</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
            <div className="transferSwiftLeftInputWrapper">
              <label className="transferSwiftLeftLabel">Expiry Date</label>
              <input type="text" className="transferSwiftLeftInput" />
            </div>
          </div>
          <div className="transferCashLeftScanWrapper">
            <select name="" id="" className="transferCashLeftScanIDDropdown">
              <option value="id" defaultChecked>
                Identity Card
              </option>
              <option value="ozkan">Ozkan Selcuk</option>
            </select>
            <div className="transferCashScanDocumentWrapper">Scan Document</div>
            <img
              src={require(`../../../assets/qr.png`)}
              alt=""
              className="transferCashLeftScanIDDropdownIcon"
            />
          </div>
          <img
            src="https://images.unsplash.com/photo-1506863530036-1efeddceb993?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1644&q=80"
            alt=""
            className="transferCashIDCardImg"
          />
          <div className="transferMainCommentRightWrapper">
            <label className="transferSwiftLeftLabel">Comment</label>
            <input className="transferSwiftLeftText" />
          </div>
        <button className="transferMainRightBottomButton">Confirm</button>
        </div>
      </div>
    </div>
  );
};

export default TransferMain;
