import React, { useState } from "react";
import "./transfer.css";
import TransferCash from "./TransferCash/TransferCash";
import TransferMain from "./TransferMain/TransferMain";
import TransferSwift from "./TransferSwift/TransferSwift";

const Transfer = () => {
  const [transferType, setTransferType] = useState("swift");

  const headerDummyData = [
    {
      id: 1,
      title: "AVAILABLE",
      subtitle: "USD Tether",
      info: "$67.032",
    },
    {
      id: 2,
      title: "Exchanges",
      subtitle: "Today",
      info: 76,
    },
    {
      id: 3,
      title: "Profit",
      subtitle: "Today",
      info: "$23.135",
    },
    {
      id: 4,
      title: "Clients",
      subtitle: "Today",
      info: 87,
    },
    {
      id: 5,
      title: "Balance",
      subtitle: "Estimate Balance",
      info: "$215.54",
    },
  ];

  return (
    <div className="transfer">
      <div className="transferHeader">
        {headerDummyData.map((header) => (
          <div key={header.id} className="transferHeaderItems">
            <h4 className="transferHeaderTitle">{header.title}</h4>
            <p className="transferHeaderSubtitle">{header.subtitle}</p>
            <div className="transferHeaderLine"></div>
            <span className="transferHeaderInfo">{header.info}</span>
          </div>
        ))}
      </div>
      <input type="text" className="exchangeChartInput" placeholder="Search" />
      {transferType === "swift" ? (
        <TransferSwift setTransferType={setTransferType} />
      ) : (
        <TransferCash setTransferType={setTransferType} />
      )}
      <TransferMain />
    </div>
  );
};

export default Transfer;
