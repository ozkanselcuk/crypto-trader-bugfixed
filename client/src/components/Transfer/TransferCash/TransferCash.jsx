import React from "react";
import "./transferCash.css";

const TransferCash = ({ setTransferType }) => {
  return (
    <div className="transferCash">
      <div className="transferCashWrapper">
        <div className="transferCashLeft">
          <div className="transferCashLeftDropdownWrapper">
            <select name="" id="" className="transferCashLeftDropdownSelect">
              <option value="ermira" defaultChecked>
                Ermira Mehmeti
              </option>
              <option value="ozkan">Ozkan Selcuk</option>
            </select>
            <img
              src={require(`../../../assets/addpage.png`)}
              alt=""
              className="transferCashLeftDropdownIcon"
            />
          </div>
          <div className="transferCashLeftScanWrapper">
            <select name="" id="" className="transferCashLeftScanIDDropdown">
              <option value="id" defaultChecked>
                Identity Card
              </option>
              <option value="ozkan">Ozkan Selcuk</option>
            </select>
            <div className="transferCashScanDocumentWrapper">Scan Document</div>
            <img
              src={require(`../../../assets/qr.png`)}
              alt=""
              className="transferCashLeftScanIDDropdownIcon"
            />
          </div>
          <img
            src="https://images.unsplash.com/photo-1506863530036-1efeddceb993?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1644&q=80"
            alt=""
            className="transferCashIDCardImg"
          />
          <div className="transferCashLeftInputContainer">
            <div className="transferCashLeftInputWrapper">
              <label className="transferCashLeftLabel">ID Number</label>
              <input type="text" className="transferCashLeftInput" />
            </div>
            <div className="transferCashLeftInputWrapper">
              <label className="transferCashLeftLabel">Expiry Date</label>
              <input type="text" className="transferCashLeftInput" />
            </div>
            <div className="transferCashLeftInputWrapper">
              <label className="transferCashLeftLabel">Address</label>
              <input type="text" className="transferCashLeftInput" />
            </div>
            <div className="transferCashLeftInputWrapper">
              <label className="transferCashLeftLabel">City</label>
              <input type="text" className="transferCashLeftInput" />
            </div>
            <div className="transferCashLeftInputWrapper">
              <label className="transferCashLeftLabel">ZIP Code</label>
              <input type="text" className="transferCashLeftInput" />
            </div>
            <div className="transferCashLeftInputWrapper">
              <label className="transferCashLeftLabel">Country</label>
              <input type="text" className="transferCashLeftInput" />
            </div>
          </div>
          <div className="transferCashCommentContainer">
            <div className="transferCashCommentWrapper">
              <label className="transferCashLeftLabel">Comment</label>
              <input className="transferCashLeftText" />
            </div>
            <img
              src={require(`../../../assets/printer.png`)}
              alt=""
              className="transferCashLeftPrintIcon"
            />
          </div>
        </div>
        <div className="transferCashLine"></div>
        <div className="transferCashRight">
          <div className="transferCashRightWrapper">
            <div className="transferCashLeftInputWrapper">
              <label className="transferCashLeftLabel">
                Full Name (Receiver)
              </label>
              <input type="text" className="transferCashLeftInput" />
            </div>
            <div className="transferCashLeftInputWrapper">
              <select
                onChange={(event) => setTransferType(event.target.value)}
                className="transferCashRightDropdown"
              >
                <option value="cash">Cash</option>
                <option value="swift">Swift</option>
              </select>
            </div>
            <div className="transferCashLeftInputWrapper">
              <label className="transferCashLeftLabel">Country</label>
              <input type="text" className="transferCashLeftInput" />
            </div>
            <div className="transferCashLeftInputWrapper">
              <label className="transferCashLeftLabel">City</label>
              <input type="text" className="transferCashLeftInput" />
            </div>
            <div className="transferCashLeftInputWrapper">
              <label className="transferCashLeftLabel">Location</label>
              <input type="text" className="transferCashLeftInput" />
            </div>
            <div className="transferCashLeftInputWrapper">
              <label className="transferCashLeftLabel">Telephone Number</label>
              <input type="text" className="transferCashLeftInput" />
            </div>
          </div>
          <div className="transferCashRightAmountContainer">
            <div className="transferCashRightAmountSendWrapper">
              <img
                src={require(`../../../assets/euro.png`)}
                alt=""
                className="transferCashRightAmountSendIcon"
              />
              <div className="transferCashRightAmountSendCurrency">
                <p className="transferCashRightAmountSendCurrencyP">You Send</p>
                <span className="transferCashRightAmountSendCurrencySpan">
                  Euro
                </span>
              </div>
              <div className="transferCashRightAmountSendCurrencyAmountWrapper">
                <span className="transferCashRightAmountSendCurrencyAmountSpan">
                  Amount:
                </span>
                <div className="transferCashRightAmountSendCurrencyAmountValue">
                  <span className="transferCashRightAmountSendCurrencyAmountValueSpan">
                    2,400.
                  </span>
                  <p className="transferCashRightAmountSendCurrencyAmountValueP">
                    00
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="transferCashRightFeeContainer">
            <span className="transferCashRightFeeSpan">Inner Fee:</span>
            <div className="transferCashRightFeeWrapper">0.4%</div>
            <span className="transferCashRightFeeSpan">Fee:</span>
            <div className="transferCashRightFeeWrapper">0.9%</div>
          </div>
          <div className="transferCashRightAmountContainer">
            <div className="transferCashRightAmountSendWrapper">
              <img
                src={require(`../../../assets/euro.png`)}
                alt=""
                className="transferCashRightAmountSendIcon"
              />
              <div className="transferCashRightAmountSendCurrency">
                <p className="transferCashRightAmountSendCurrencyP">You Pay</p>
                <span className="transferCashRightAmountSendCurrencySpan">
                  Euro
                </span>
              </div>
              <div className="transferCashRightAmountSendCurrencyAmountWrapper">
                <span className="transferCashRightAmountSendCurrencyAmountSpan">
                  Amount:
                </span>
                <div className="transferCashRightAmountSendCurrencyAmountValue">
                  <span className="transferCashRightAmountSendCurrencyAmountValueSpan">
                    2,400.
                  </span>
                  <p className="transferCashRightAmountSendCurrencyAmountValueP">
                    00
                  </p>
                </div>
              </div>
            </div>
          </div>
          <button className="transferCashRightBottomButton">Confirm</button>
        </div>
      </div>
    </div>
  );
};

export default TransferCash;
