import React, { useEffect, useState } from "react";
// React Calander
import Calendar from "react-calendar";
// CSS
import "./rightSidebar.css";
// Message Box
import { Widget } from "react-chat-widget";
import "react-chat-widget/lib/styles.css";

const RightSidebar = ({ setPage, socket }) => {
  const [date, setDate] = useState("");
  const [hour, setHour] = useState("");
  const [more, setMore] = useState(false);
  const [messenger, setMessenger] = useState(false);
  const [submit, setSubmit] = useState(null);
  const [value, onChangeClander] = useState(new Date());
  // Socket.io State
  const [notification, setNotification] = useState([]);
  const [notificationText, setNotificationText] = useState("");
  const [notificationType, setNotificationType] = useState(0);

  useEffect(() => {
    socket?.on("getNotification", (data) => {
      setNotification(data);
      console.log(data);
    });
  }, [socket]);

  const handleCreateNotification = (func) => {
    socket.emit("sendNotification", {
      func,
      type: notificationType,
      text: notificationText,
    });
  };

  const handleRemoveNotification = (func, text) => {
    socket.emit("sendNotification", {
      func,
      text,
    });
  };

  const handleDate = () => {
    var days = new Array(
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    );

    var months = new Array(
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    );

    var now = new Date();

    const minutes =
      now.getMinutes() < 10 ? `0${now.getMinutes()}` : now.getMinutes();

    const seconds =
      now.getSeconds() < 10 ? `0${now.getSeconds()}` : now.getSeconds();

    setDate(
      `${days[now.getDay()]}, ${now.getDate()} ${months[now.getMonth()]}`
    );

    setHour(`${now.getHours()}:${minutes}:${seconds}`);
  };

  setInterval(handleDate, 1000);

  return (
    <div className="rightSidebar">
      <div className="rightSidebarCalander">
        <div className="rightSidebarCalanderItem">
          <div className="rightSidebarCalanderItemWrapper">
            <span className="rightSidebarCalanderItemDateTopSpan">{date}</span>
            <div className="rightSidebarCalanderItemDateLine"></div>
            <span className="rightSidebarCalanderItemDateBottomSpan">
              {hour}
            </span>
          </div>
          <div className="rightSidebarCalanderItemWrapper">
            <span className="rightSidebarCalanderItemDateTopSpan">
              Daily Fee
            </span>
            <div className="rightSidebarCalanderItemDateLine"></div>
            <span className="rightSidebarCalanderItemDateBottomSpan">2.5%</span>
          </div>
        </div>
      </div>
      <div className="rightSidebarWrapper">
        <Calendar onChange={onChangeClander} value={value} />
        <div className="rightSidebarNotification">
          <div className="rightSidebarNotificationWrapper">
            {notification.map((item, index) =>
              more ? (
                <div key={item.id} className="rightSidebarNotificationItem">
                  <img
                    className="rightSidebarNotificationIcon"
                    src={
                      item.type === 1
                        ? require(`../../assets/notification.png`)
                        : require(`../../assets/pin.png`)
                    }
                    alt=""
                  />
                  <span className="rightSidebarNotificationSpan">
                    {item.text}
                  </span>
                  <span
                    onClick={() => handleRemoveNotification(1, item.text)}
                    style={{ cursor: "pointer" }}
                  >
                    {"  "}X
                  </span>
                </div>
              ) : (
                index < 4 && (
                  <div key={item.id} className="rightSidebarNotificationItem">
                    <img
                      className="rightSidebarNotificationIcon"
                      src={
                        item.type === 1
                          ? require(`../../assets/notification.png`)
                          : require(`../../assets/pin.png`)
                      }
                      alt=""
                    />
                    <span className="rightSidebarNotificationSpan">
                      {item.text}
                    </span>

                    <span
                      onClick={() => handleRemoveNotification(1, item.text)}
                      style={{ cursor: "pointer" }}
                    >
                      {"  "}X
                    </span>
                  </div>
                )
              )
            )}
            <button
              className="rightSidebarNotificationMoreButton"
              onClick={() => setMore(!more)}
            >
              {more ? "Show Less" : "Show More"}
            </button>
          </div>
        </div>
      </div>
      <input
        type="text"
        onChange={(event) => setNotificationText(event.target.value)}
        value={notificationText}
      />
      <select
        onChange={(event) => setNotificationType(parseInt(event.target.value))}
      >
        <option value="0">Pin</option>
        <option value="1">Notification</option>
      </select>
      <button onClick={() => handleCreateNotification(0)}>socket</button>
      <button onClick={() => setMessenger(!messenger)}>Support</button>
      <button onClick={() => setPage("Messages")}>Messages</button>
      {messenger && (
        <Widget
          title="Ultrasoft"
          subtitle="Ultrasoft Message Box"
          handleSubmit={(event) => setSubmit(event)}
          emojis={true}
          showBadge={true}
        />
      )}
    </div>
  );
};

export default RightSidebar;
