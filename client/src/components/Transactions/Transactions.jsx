import React from "react";
import "./transactions.css";

const Transactions = () => {
  const headerDummyData = [
    {
      id: 1,
      title: "AVAILABLE",
      subtitle: "USD Tether",
      info: "$67.032",
    },
    {
      id: 2,
      title: "Exchanges",
      subtitle: "Today",
      info: 76,
    },
    {
      id: 3,
      title: "Profit",
      subtitle: "Today",
      info: "$23.135",
    },
    {
      id: 4,
      title: "Clients",
      subtitle: "Today",
      info: 87,
    },
    {
      id: 5,
      title: "Balance",
      subtitle: "Estimate Balance",
      info: "$215.54",
    },
  ];

  return (
    <div className="transactions">
      <div className="transactionsHeader">
        {headerDummyData.map((header) => (
          <div key={header.id} className="transactionsHeaderItems">
            <h4 className="transactionsHeaderTitle">{header.title}</h4>
            <p className="transactionsHeaderSubtitle">{header.subtitle}</p>
            <div className="transactionsHeaderLine"></div>
            <span className="transactionsHeaderInfo">{header.info}</span>
          </div>
        ))}
      </div>
      <input type="text" className="exchangeChartInput" placeholder="Search" />
    </div>
  );
};

export default Transactions;
