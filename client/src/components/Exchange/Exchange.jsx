import React, { useState } from "react";
// Components
import BottomExchange from "./BottomExchange/BottomExchange";
import ExchangeChart from "./ExchangeChart/ExchangeChart";
// CSS
import "./exchange.css";
import ExchangeMain from "./ExcangeMain/ExchangeMain";

const Exchange = () => {
  const [clicked, setClicked] = useState(false);

  const headerDummyData = [
    {
      id: 1,
      title: "AVAILABLE",
      subtitle: "USD Tether",
      info: "$67.032",
    },
    {
      id: 2,
      title: "Exchanges",
      subtitle: "Today",
      info: 76,
    },
    {
      id: 3,
      title: "Profit",
      subtitle: "Today",
      info: "$23.135",
    },
    {
      id: 4,
      title: "Clients",
      subtitle: "Today",
      info: 87,
    },
    {
      id: 5,
      title: "Balance",
      subtitle: "Estimate Balance",
      info: "$215.54",
    },
  ];

  const dummyData = [
    {
      id: 1,
      icon: "tether",
      title: "Besim Dalloshi",
      subtitle: "Debit",
      value: 321,
      time: "23:12:42",
      status: 0,
    },
    {
      id: 2,
      icon: "bitcoin",
      title: "Besim Dalloshi",
      subtitle: "Deposit",
      value: 3214,
      time: "12:22:42",
      status: 1,
    },
    {
      id: 3,
      icon: "ethereum",
      title: "Besim Dalloshi",
      subtitle: "Convert",
      value: 65,
      time: "15:15:32",
      status: 1,
    },
    {
      id: 4,
      icon: "solana",
      title: "Besim Dalloshi",
      subtitle: "Deposit",
      value: 765,
      time: "23:12:42",
      status: 2,
    },
    {
      id: 5,
      icon: "binance-coin",
      title: "Binance Besim Dalloshi",
      subtitle: "Convert",
      value: 34,
      time: "15:15:32",
      status: 0,
    },
    {
      id: 6,
      icon: "tether",
      title: "Besim Dalloshi",
      subtitle: "Debit",
      value: 321,
      time: "23:12:42",
      status: 0,
    },
    {
      id: 7,
      icon: "bitcoin",
      title: "Besim Dalloshi",
      subtitle: "Debit",
      value: 3214,
      time: "12:22:42",
      status: 1,
    },
    {
      id: 8,
      icon: "ethereum",
      title: "Besim Dalloshi",
      subtitle: "Deposit",
      value: 65,
      time: "15:15:32",
      status: 2,
    },
  ];

  return (
    <div className="exchange">
      <div className="exchangeHeader">
        {headerDummyData.map((header) => (
          <div key={header.id} className="exchangeHeaderItems">
            <h4 className="exchangeHeaderTitle">{header.title}</h4>
            <p className="exchangeHeaderSubtitle">{header.subtitle}</p>
            <div className="exchangeHeaderLine"></div>
            <span className="exchangeHeaderInfo">{header.info}</span>
          </div>
        ))}
      </div>
      <input type="text" className="exchangeChartInput" placeholder="Search" />
      {clicked ? <ExchangeMain /> : <ExchangeChart setClicked={setClicked} />}
      <input type="text" className="exchangeChartInput" placeholder="Search" />
      <div className="exchangeBottom">
        {dummyData.map((data) => (
          <div className="exchangeBottomWrapper">
            <BottomExchange
              id={data.id}
              icon={data.icon}
              title={data.title}
              subtitle={data.subtitle}
              value={data.value}
              status={data.status}
              time={data.time}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default Exchange;
