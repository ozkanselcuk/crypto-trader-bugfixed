import React from "react";
import "./bottomExchange.css";
// Sweet Alert 2
import Swal from "sweetalert2";

const BottomExchange = ({ id, icon, title, status, subtitle, time, value }) => {
  const handlePendingClick = (status) => {
    if (status === 0) {
      Swal.fire({
        title: "Do you want to accept the Exchange Request?",
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: "Accept",
        denyButtonText: `Refuse`,
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("Accepted the Exchange Request!", "", "success");
        } else if (result.isDenied) {
          Swal.fire("Refused the Exchange Request", "", "info");
        }
      });
    } else {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "You are already change status of this Exchange Request!",
        footer:
          '<a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ&ab_channel=RickAstley">Something is wrong? Do you wanna contact with the Support?</a>',
      });
    }
  };

  return (
    <div className="bottomExchange">
      <div className="bottomExchangeWrapper">
        <div key={id} className="bottomExchange">
          <div className="bottomExchangeItem">
            <div className="bottomExchangeItemInfoIconContainer">
              <img
                className="bottomExchangeItemIcon"
                src={require(`../../../assets/src/${icon}.png`)}
                alt=""
              />
            </div>
            <div className="bottomExchangeItemInfo">
              <div className="bottomExchangeItemInfoWrapper">
                <span className="bottomExchangeItemInfoSpan">{title}</span>
                <p className="bottomExchangeItemInfoP">{subtitle}</p>
              </div>
              <span className="bottomExchangeItemInfoValue">{value}</span>
            </div>
          </div>
          <div className="bottomExchangeItemButtonContainer">
            <div className="bottomExchangeItemInfoButtonContainerWrapper">
              <div
                className="bottomExchangeItemInfoButtonContainerButton"
                style={
                  status === 0
                    ? { backgroundColor: "orange" }
                    : status === 1
                    ? { backgroundColor: "green" }
                    : { backgroundColor: "red" }
                }
              >
                <span className="bottomExchangeItemInfoButtonContainerButtonSpan">
                  {status === 0 && "PENDING"}
                  {status === 1 && "ACCEPTED"}
                  {status === 2 && "DENIED"}
                </span>
              </div>
              <img
                onClick={() => handlePendingClick(status)}
                src={require("../../../assets/edit.png")}
                alt=""
                className="bottomExchangeItemInfoButtonContainerButtonIcon"
              />
            </div>
            <div className="bottomExchangeItemButtonContainerTime">
              <span>🕓</span>
              <span style={{ marginLeft: "5px", color: "gray" }}>{time}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BottomExchange;
