import React from "react";
// React Chart
import { LineChart, Line, ResponsiveContainer } from "recharts";
import "./exchangeChart.css";

const ExchangeChart = ({ setClicked }) => {
  const rechartData = [
    { name: "Page B", uv: 200, pv: 200, amt: 2000 },
    { name: "Page C", uv: 400, pv: 500, amt: 1800 },
    { name: "Page D", uv: 300, pv: 300, amt: 1500 },
    { name: "Page E", uv: 400, pv: 100, amt: 1200 },
    { name: "Page 2", uv: 340, pv: 500, amt: 1800 },
    { name: "Page 3", uv: 360, pv: 300, amt: 1500 },
    { name: "Page 4", uv: 350, pv: 100, amt: 1200 },
    { name: "Page 5", uv: 410, pv: 500, amt: 1800 },
    { name: "Page 6", uv: 400, pv: 300, amt: 1500 },
    { name: "Page 7", uv: 380, pv: 100, amt: 1200 },
    { name: "Page 8", uv: 400, pv: 500, amt: 1800 },
  ];

  const rechart = (
    <ResponsiveContainer width="100%" height={100}>
      <LineChart data={rechartData}>
        <Line dataKey="uv" stroke="#C0392B" dot={false} />
      </LineChart>
    </ResponsiveContainer>
  );

  const chartDummyData = [
    {
      id: 1,
      title: "TETHER",
      icon: "tether",
      subtitle: "USD Tether",
      change: 6.8,
      sell: 0.99,
      buy: 1.01,
    },
    {
      id: 2,
      title: "BNB",
      icon: "binance-coin",
      subtitle: "Binance Coin",
      change: 1.2,
      sell: 624.69,
      buy: 651.82,
    },
    {
      id: 3,
      title: "BTC",
      icon: "bitcoin",
      subtitle: "Bitcoin",
      change: 6.1,
      sell: 45231.21,
      buy: 47213.56,
    },
    {
      id: 4,
      title: "ETH",
      icon: "ethereum",
      subtitle: "Ethereum",
      change: 4.7,
      sell: 4672.98,
      buy: 4782.2,
    },
    {
      id: 5,
      title: "SOL",
      icon: "solana",
      subtitle: "Solana",
      change: 2.7,
      sell: 252.89,
      buy: 265.87,
    },
    {
      id: 6,
      title: "TETHER",
      icon: "tether",
      subtitle: "USD Tether",
      change: 6.8,
      sell: 0.99,
      buy: 1.01,
    },
    {
      id: 7,
      title: "BNB",
      icon: "binance-coin",
      subtitle: "Binance Coin",
      change: 1.2,
      sell: 624.69,
      buy: 651.82,
    },
    {
      id: 8,
      title: "BTC",
      icon: "bitcoin",
      subtitle: "Bitcoin",
      change: 6.1,
      sell: 45231.21,
      buy: 47213.56,
    },
  ];

  return (
    <div className="exchangeChart">
      <div className="exchangeChartWrapper">
        <div className="exhangeFirstChart">
          <div className="exchangeFirstChartHeader">
            <div className="exchangeFirstChartHeaderLeft">
              <div className="exchangeFirstChartImgContainer">
                <img
                  src={require(`../../../assets/eur.png`)}
                  alt=""
                  className="exchangeFirstChartHeaderIcon"
                />
                <div className="exchangeFirstChartSmallImgContainer">
                  <img
                    src={require(`../../../assets/yen.png`)}
                    alt=""
                    className="exchangeFirstChartHeaderSmallIcon"
                  />
                </div>
              </div>
              <span className="exchangeFirstChartHeaderTitle">EUR / TRY</span>
            </div>
            <div className="exchangeFirstChartHeaderRight">
              <span>132.500.</span>
              <span className="exchangeFirstChartHeaderRightP">00</span>
            </div>
          </div>
          <div className="exchangeFirstChartWrapper">
            <div className="exchangeFirstChartLeft">
              <div className="exchangeFirstChartItems">
                <div className="exchangeChartHeaderChangeWrapper">
                  <span className="exchangeChartHeaderChangeTitle">
                    CHANGE (24H)
                  </span>
                  <p className="exchangeChartHeaderChangeCalc">
                    -{chartDummyData[0].change}%
                  </p>
                </div>
                <div className="exchangeChartGraph">{rechart}</div>
                <div className="exchangeChartBuySellWrapper">
                  <div className="exchangeChartSell">
                    <span className="exchangeChartSellTitle">SELL</span>
                    <p className="exchangeChartSellPrice">
                      {chartDummyData[0].sell}
                    </p>
                  </div>
                  <div className="exchangeChartBuy">
                    <span className="exchangeChartBuyTitle">BUY</span>
                    <p className="exchangeChartBuyPrice">
                      {chartDummyData[0].buy}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="exchangeFirstChartRight">
              <div className="exchangeFirstChartRightTop">
                <div className="exchangeFirstChartRightTopLeft">
                  <img
                    src={require(`../../../assets/euro.png`)}
                    alt=""
                    className="exchangeFirstChartRightTopIcon"
                  />
                  <span className="exchangeFirstChartRightTopSpan">EUR</span>
                </div>
                <div className="exchangeFirstChartRightTopValueWrapper">
                  <span className="exchangeFirstChartRightTopValue">
                    50.144.
                  </span>
                  <span className="exchangeFirstChartRightTopValueSmall">
                    00
                  </span>
                </div>
              </div>
              <div className="exchangeFirstChartRightMiddle">
                <div className="exchangeFirstChartRightMiddleLeft">
                  <img
                    src={require(`../../../assets/transferCurrency.png`)}
                    alt=""
                    className="exchangeFirstChartRightMiddleIcon"
                  />
                </div>
                <div className="exchangeFirstChartRightMiddleValueWrapper">
                  <span className="exchangeFirstChartRightTopValue">61.57</span>
                </div>
              </div>
              <div className="exchangeFirstChartRightTop">
                <div className="exchangeFirstChartRightTopLeft">
                  <img
                    src={require(`../../../assets/yen.png`)}
                    alt=""
                    className="exchangeFirstChartRightTopIcon"
                  />
                  <span className="exchangeFirstChartRightTopSpan">TRY</span>
                </div>
                <div className="exchangeFirstChartRightTopValueWrapper">
                  <span className="exchangeFirstChartRightTopValue">
                    50.144.
                  </span>
                  <span className="exchangeFirstChartRightTopValueSmall">
                    00
                  </span>
                </div>
              </div>
              <div className="exchangeFirstChartRightBottom">
                <img
                  src={require(`../../../assets/printer.png`)}
                  className="exchangeFirstChartRightBottomImg"
                  alt=""
                  onClick={() => setClicked(true)}
                />
              </div>
            </div>
          </div>
        </div>
        {chartDummyData.map((item) => (
          <div className="exchangeChartItems">
            <div className="exchangeChartHeader">
              <div className="exchangeFirstChartHeaderLeft">
                <div className="exchangeFirstChartImgContainer">
                  <img
                    src={require(`../../../assets/eur.png`)}
                    alt=""
                    className="exchangeFirstChartHeaderIcon"
                  />
                  <div className="exchangeFirstChartSmallImgContainer">
                    <img
                      src={require(`../../../assets/src/${item.icon}.png`)}
                      alt=""
                      className="exchangeFirstChartHeaderSmallIcon"
                    />
                  </div>
                </div>
                <span className="exchangeFirstChartHeaderTitle">EUR / MKD</span>
              </div>
            </div>
            <div className="exchangeChartHeaderChangeWrapper">
              <span className="exchangeChartHeaderChangeTitle">
                CHANGE (24H)
              </span>
              <p className="exchangeChartHeaderChangeCalc">-{item.change}%</p>
            </div>
            <div className="exchangeChartGraph">{rechart}</div>
            <div className="exchangeChartBuySellWrapper">
              <div className="exchangeChartSell">
                <span className="exchangeChartSellTitle">SELL</span>
                <p className="exchangeChartSellPrice">{item.sell}</p>
              </div>
              <div className="exchangeChartBuy">
                <span className="exchangeChartBuyTitle">BUY</span>
                <p className="exchangeChartBuyPrice">{item.buy}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ExchangeChart;
