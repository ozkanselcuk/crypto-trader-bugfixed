import React, { useState } from "react";
// CSS
import "./dashboardSliderFirstLeft.css";
// Sweet Alert 2
import Swal from "sweetalert2";

const DashboardSliderFirst = () => {
  const dummyDataLeft = [
    {
      id: 1,
      icon: "tether",
      title: "TETHER",
      subtitle: "USD Tether",
      value: 321,
      time: "23:12:42",
      status: 0,
    },
    {
      id: 2,
      icon: "bitcoin",
      title: "BITCOIN",
      subtitle: "BTC",
      value: 3214,
      time: "12:22:42",
      status: 1,
    },
    {
      id: 3,
      icon: "ethereum",
      title: "Ethereum",
      subtitle: "ETH",
      value: 65,
      time: "15:15:32",
      status: 1,
    },
    {
      id: 4,
      icon: "solana",
      title: "Solana",
      subtitle: "SOL",
      value: 765,
      time: "23:12:42",
      status: 2,
    },
    {
      id: 5,
      icon: "binance-coin",
      title: "Binance Coin",
      subtitle: "BNB",
      value: 34,
      time: "15:15:32",
      status: 0,
    },
    {
      id: 6,
      icon: "tether",
      title: "TETHER",
      subtitle: "USD Tether",
      value: 321,
      time: "23:12:42",
      status: 0,
    },
    {
      id: 7,
      icon: "bitcoin",
      title: "BITCOIN",
      subtitle: "BTC",
      value: 3214,
      time: "12:22:42",
      status: 1,
    },
    {
      id: 8,
      icon: "ethereum",
      title: "Ethereum",
      subtitle: "ETH",
      value: 65,
      time: "15:15:32",
      status: 2,
    },
  ];

  const handlePendingClick = (status) => {
    if (status === 0) {
      Swal.fire({
        title: "Do you want to accept the Exchange Request?",
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: "Accept",
        denyButtonText: `Refuse`,
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("Accepted the Exchange Request!", "", "success");
        } else if (result.isDenied) {
          Swal.fire("Refused the Exchange Request", "", "info");
        }
      });
    } else {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "You are already change status of this Exchange Request!",
        footer:
          '<a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ&ab_channel=RickAstley">Something is wrong? Do you wanna contact with the Support?</a>',
      });
    }
  };

  return (
    <div className="dashboardSliderFirst">
      <div className="dashboardSliderFirstWrapper">
        {dummyDataLeft.map((data) => (
          <div key={data.id} className="dashboardSliderFirstLeft">
            <div className="dashboardSliderFirstLeftItem">
              <div className="dashboardSliderFirstLeftItemInfoLeftIconContainer">
                <img
                  className="dashboardSliderFirstLeftItemIcon"
                  src={require(`../../../assets/src/${data.icon}.png`)}
                  alt=""
                />
              </div>
              <div className="dashboardSliderFirstLeftItemInfo">
                <div className="dashboardSliderFirstLeftItemInfoLeft">
                  <span className="dashboardSliderFirstLeftItemInfoLeftSpan">
                    {data.subtitle}
                  </span>
                  <p className="dashboardSliderFirstLeftItemInfoLeftP">
                    {data.title}
                  </p>
                </div>
                <span className="dashboardSliderFirstLeftItemInfoLeftValue">
                  {data.value}
                </span>
              </div>
            </div>
            <div className="dashboardSliderFirstLeftItemButtonContainer">
              <div className="dashboardSliderFirstLeftItemInfoLeftButtonContainerWrapper">
                <div
                  className="dashboardSliderFirstLeftItemInfoLeftButtonContainerButton"
                  style={
                    data.status === 0
                      ? { backgroundColor: "orange" }
                      : data.status === 1
                      ? { backgroundColor: "green" }
                      : { backgroundColor: "red" }
                  }
                >
                  <span className="dashboardSliderFirstLeftItemInfoLeftButtonContainerButtonSpan">
                    {data.status === 0 && "PENDING"}
                    {data.status === 1 && "ACCEPTED"}
                    {data.status === 2 && "DENIED"}
                  </span>
                </div>
                <img
                  onClick={() => handlePendingClick(data.status)}
                  src={require("../../../assets/edit.png")}
                  alt=""
                  className="dashboardSliderFirstLeftItemInfoLeftButtonContainerButtonIcon"
                />
              </div>
              <div className="dashboardSliderFirstLeftItemButtonContainerTime">
                <span>🕓</span>
                <span style={{ marginLeft: "5px", color: "gray" }}>
                  {data.time}
                </span>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default DashboardSliderFirst;
