import React from "react";
// React Router
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
// Pages
import MainPage from "./pages/MainPage/MainPage";
import LoginPage from "./pages/LoginPage/LoginPage";
// Redux
import { useSelector } from "react-redux";

const App = () => {
  const user = useSelector((state) => state.user.currentUser);

  return (
    <BrowserRouter>
      <>
        <Routes>
          <Route path="/" element={user ? <MainPage /> : <LoginPage />} />
          <Route path="/login" element={<LoginPage />} />
        </Routes>
      </>
    </BrowserRouter>
  );
};

export default App;
