const { Server } = require("socket.io");

const io = new Server({
  cors: {
    origin: "http://localhost:3000",
  },
});

let onlineUsers = [];
let notificationArr = [];

const addNewUser = (userId, socketId) => {
  !onlineUsers.some((user) => user.userId === userId) &&
    onlineUsers.push({ userId, socketId });
};

const removeUser = (socketId) => {
  onlineUsers = onlineUsers.filter((user) => user.socketId !== socketId);
};

const getUser = (userId) => {
  return onlineUsers.find((user) => user.userId === userId);
};

io.on("connection", (socket) => {
  console.log("a user is connected");

  socket.on("newUser", (userId) => {
    addNewUser(userId, socket.id);
  });

  socket.on("sendNotification", ({ func, type, text }) => {
    if (func === 0) {
      notificationArr.push({
        func,
        type,
        text,
      });
    } else {
      notificationArr.splice(
        notificationArr.findIndex((index) => index.text === text),
        1
      );
      // notificationArr = notificationArr.filter((arr) => arr.text !== text);
    }

    io.emit("getNotification", notificationArr);
  });

  // socket.on("sendNotification", ({ senderName, receiverName, type }) => {
  //   const receiver = getUser(receiverName);
  //   io.to(receiver?.socketId).emit("getNotification", {
  //     senderName,
  //     type,
  //   });
  // });

  socket.on("disconnect", () => {
    console.log("someone is left the socket server!");

    removeUser(socket.id);
  });
});

io.listen(8080);
